<?php

require_once dirname(__DIR__) . '/AppointmentsAPITestParent.php';
require_once dirname(dirname(__DIR__)).'/src/apis/organization/AppointmentsOrganizationAPI.php';
require_once dirname(dirname(__DIR__)).'/src/apis/organization/invite/AppointmentsUserOrgInviteEvaluationStatus.php';

class AppointmentsOrganizationAPITest extends AppointmentsAPITestParent {

	public function setUp() {
		parent::setUp();
	}

	public function testGetInviteEvaluationStatus(){

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_org_invites']);

		$request = new AppointmentsUserOrgInviteStatusRequest('tester1@app.appointments.timerocket.com', 1);
		$response = AppointmentsOrganizationAPI::getInviteStatusOfEmails($request);
		$this->assertTrue($response->success, $response->message);

		$invite_items = $response->getUserOrgInviteData()->getUserOrgInviteStatuses();

		$this->assertCount(1, $invite_items);

		$invite_item = $invite_items[0];

		$this->assertEquals(AppointmentsUserOrgInviteEvaluationStatus::ALREADY_IN_ORG, $invite_item->getStatus());
	}

	public function testGetInviteStatusWithEmptyRequest(){
		$request = new AppointmentsUserOrgInviteStatusRequest(',', 1);
		$response = AppointmentsOrganizationAPI::getInviteStatusOfEmails($request);
		$this->assertTrue($response->success, $response->message);
		$this->assertTrue(is_array($response->data));
	}

	public function testInvite() {
		$request = new AppointmentsUserOrgInviteRequest('tester1@app.timerocket.com test2@gmail.com', 1, true);

		$request->setFromName('Jim');
		$request->setFromEmail('jim@bim.co');
		$request->setAcceptRedirect('http://accept.com');
		$request->setDeclineRedirect('http://decline.com');
		$request->setMakeManager(true);
		$request->setNotifyEmail('sara@fara.wara');
		$request->setMessageID(88);
		$request->setAcceptRedirect('http://google.com');
		$request->setDeclineRedirect('http://yahoo.com');
		$request->setNotifyEmail('abc@123.xyz');
		$request->setUseDefaultMessage(false);

		$response = AppointmentsOrganizationAPI::invite($request);
		$this->assertTrue($response->success, $response->message);
	}

	public function testRejectInvite() {

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_org_invites']);
		$this->assertTrue($result->success, $result->message);

		$request = new AppointmentsUserOrgInviteAcceptRequest(1, 'abc123');
		$response = AppointmentsOrganizationAPI::acceptInvite($request);
		$this->assertTrue($response->success, $response->message);

		$this->assertEquals(AppointmentsUserOrgInviteStatus::ACCEPTED, $response->getOrgInviteEvaluation()->getStatus());
	}

	public function testAcceptInvite() {

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_org_invites']);
		$this->assertTrue($result->success, $result->message);

		$request = new AppointmentsUserOrgInviteAcceptRequest(1, 'abc123');

		$response = AppointmentsOrganizationAPI::acceptInvite($request);
		$this->assertTrue($response->success, $response->message);

		$this->assertEquals(AppointmentsUserOrgInviteStatus::ACCEPTED, $response->getOrgInviteEvaluation()->getStatus());
	}

}