<?php

require_once dirname(__DIR__) . '/AppointmentsAPITestParent.php';
require_once dirname(dirname(__DIR__)).'/src/apis/user/AppointmentsUserAPI.php';

class AppointmentsUserAPITest extends AppointmentsAPITestParent {

	public function testLogin() {

		$restore_data_response = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_meetings'], true);

		$this->assertTrue($restore_data_response->success, $restore_data_response->message);

		$request = new AppointmentsLoginRequest('tester1@app.appointments.timerocket.com', 'I love testing');

		$response = AppointmentsUserAPI::login($request);

		$this->assertTrue($response->success, $response->message);
		$this->assertEquals(1, $response->getUserData()->getPerson()->getUserId());
	}

}