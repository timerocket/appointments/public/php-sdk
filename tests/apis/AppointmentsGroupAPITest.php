<?php

require_once dirname(__DIR__) . '/AppointmentsAPITestParent.php';
require_once dirname(dirname(__DIR__)).'/src/apis/group/AppointmentsGroupAPI.php';

class AppointmentsGroupAPITest extends AppointmentsAPITestParent {

	public function setUp() {
		parent::setUp();
	}

	public function testGetInviteEvaluationStatus(){

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_groups']);
		$this->assertTrue($result->success, $result->message);
		
		$request = new AppointmentsGroupInviteStatusRequest('tester1@app.appointments.timerocket.com', 1);
		$response = AppointmentsGroupAPI::getInviteStatusOfEmails($request);
		$this->assertTrue($response->success, $response->message);
		
		$invite_items = $response->getGroupInviteData()->getGroupInviteStatuses();

		$this->assertCount(1, $invite_items);

		$invite_item = $invite_items[0];

		$this->assertEquals(AppointmentsGroupInviteEvaluationStatus::ALREADY_IN_GROUP, $invite_item->getStatus());
	}

	public function testGetInviteStatusWithEmptyRequest(){
		$request = new AppointmentsGroupInviteStatusRequest(',', 1);
		$response = AppointmentsGroupAPI::getInviteStatusOfEmails($request);
		$this->assertTrue($response->success, $response->message);
		$this->assertTrue(is_array($response->data));
	}

	public function testInvite() {
		$request = new AppointmentsGroupInviteRequest('tester1@app.timerocket.com test2@gmail.com', 1, true);

		$request->setFromName('Jim');
		$request->setFromEmail('jim@bim.co');
		$request->setAcceptRedirect('http://accept.com');
		$request->setDeclineRedirect('http://decline.com');
		$request->setMakeManager(true);
		$request->setNotifyEmail('sara@fara.wara');
		$request->setMessageID(88);
		$request->setAlsoInviteToOrg(true);
		$request->setAcceptRedirect('http://google.com');
		$request->setDeclineRedirect('http://yahoo.com');
		$request->setNotifyEmail('abc@123.xyz');
		$request->setUseDefaultMessage(false);

		$response = AppointmentsGroupAPI::invite($request);
		$this->assertTrue($response->success, $response->message);
	}

	public function testRejectInvite() {

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_groups', 'make_group_invites']);
		$this->assertTrue($result->success, $result->message);

		$request = new AppointmentsGroupInviteAcceptRequest(1, 'abc.123');
		$response = AppointmentsGroupAPI::acceptInvite($request);
		$this->assertTrue($response->success, $response->message);

		$this->assertEquals(AppointmentsGroupInviteStatus::ACCEPTED, $response->getGroupInviteEvaluation()->getStatus());
	}

	public function testAcceptInvite() {

		$result = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_groups', 'make_group_invites']);
		$this->assertTrue($result->success, $result->message);

		$request = new AppointmentsGroupInviteAcceptRequest(1, 'abc.123');

		$response = AppointmentsGroupAPI::acceptInvite($request);
		$this->assertTrue($response->success, $response->message);

		$this->assertEquals(AppointmentsGroupInviteStatus::ACCEPTED, $response->getGroupInviteEvaluation()->getStatus());
	}

}