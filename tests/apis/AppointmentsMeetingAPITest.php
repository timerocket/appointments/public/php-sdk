<?php

/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 9:14 PM
 */

require_once dirname(__DIR__) . '/AppointmentsAPITestParent.php';
require_once dirname(dirname(__DIR__)).'/src/apis/meeting/AppointmentsMeetingAPI.php';

class AppointmentsMeetingAPITest extends AppointmentsAPITestParent {

	public function testCreateMeeting() {

		$restore_data_response = AppointmentsSDKTestHelper::getDataResetResponse(['make_users', 'make_meetings'], true);

		$this->assertTrue($restore_data_response->success, $restore_data_response->message);

		$start   = time() + 3600;
		$end     = $start + 3600;
		$created = time();

		$meeting_attendees = [
			['user_id' => 1, 'status' => 6, 'first_accepted' => 1458246858],
			['user_id' => 2, 'status' => 6, 'first_accepted' => 1458246858],
		];

		$request = new AppointmentsCreateMeetingRequest(
			1, 1, $start, $end, $created, $meeting_attendees
		);

		$response = AppointmentsMeetingAPI::create($request);

		$this->assertTrue($response->success, $response->message);
		$this->assertEquals(2, $response->getMeetingData()->getMeetingID());

		$request = new AppointmentsCreateMeetingRequest(
			1, 1, $start, $end, $created, [1,2]
		);

		$response = AppointmentsMeetingAPI::create($request);

		$this->assertFalse($response->success);

		$request = new AppointmentsCreateMeetingRequest(
			1, 1, $start + 3600, $end + 3600, $created, [1,2]
		);

		$response = AppointmentsMeetingAPI::create($request);

		$this->assertTrue($response->success, $response->message);
		$this->assertEquals(3, $response->getMeetingData()->getMeetingID());
	}

}