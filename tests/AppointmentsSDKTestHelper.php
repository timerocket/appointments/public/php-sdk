<?php

/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 9:23 PM
 */

require_once dirname(__DIR__).'/src/api/AppointmentsAPIRequest.php';

class AppointmentsSDKTestHelper {

	/**
	 * @param array   $script_names
	 * @param boolean $reset_cache
	 *
	 * @return AppointmentsAPIResponse
	 */
	public static function getDataResetResponse(array $script_names, $reset_cache=true) {

		$scripts             = implode(',', $script_names);
		$reset_cache_command = $reset_cache ? 'YES' : 'NO';

		$parameters = array(
			'clear_cache' => $reset_cache_command,
			'api_key'     => AppointmentsSDK::getKey(),
			'script'      => $scripts
		);

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getServer() . '/tests/data_restore.php',
			'GET',
			$parameters
		);

		return new AppointmentsAPIResponse($request->getResponse());
	}

	public static function getAPIKey(){
		
		global $appointments_api_config;

		$api_key = $appointments_api_config['api_key'];

		if(!in_array(trim($api_key), array('api key goes here', ''))){
			return $api_key;
		}else{
			throw new Exception('You must set your TimeRocket™ Appointments api key in tests/test_config.php');
		}
	}

	public static function getTestServer(){

		global $appointments_api_config;

		$api_key = $appointments_api_config['server'];

		if(!in_array(trim($api_key), array('server goes here', ''))){
			return $api_key;
		}else{
			throw new Exception('You must set your TimeRocket™ Appointments test server in tests/test_config.php');
		}
	}

}