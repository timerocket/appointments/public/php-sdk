<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 9:38 PM
 */

require_once dirname(__DIR__).'/autoload.php';
require_once __DIR__.'/AppointmentsSDKTestHelper.php';
require_once __DIR__.'/test_config.php';

class AppointmentsAPITestParent extends PHPUnit_Framework_TestCase {

	public function setUp() {
		parent::setUp();
		AppointmentsSDK::initService(AppointmentsSDKTestHelper::getAPIKey(), true);
		AppointmentsSDK::getService()->setServer(AppointmentsSDKTestHelper::getTestServer());
		AppointmentsSDK::getService()->setPlainTextCredentials(1, 'I love testing');
		AppointmentsSDK::getService()->disableMessaging();
	}
}