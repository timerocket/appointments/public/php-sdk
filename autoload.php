<?php

require_once __DIR__.'/src/api/AppointmentsSDK.php';
require_once __DIR__.'/src/apis/meeting/AppointmentsMeetingAPI.php';
require_once __DIR__.'/src/apis/organization/AppointmentsOrganizationAPI.php';
require_once __DIR__.'/src/apis/group/AppointmentsGroupAPI.php';
require_once __DIR__.'/src/apis/user/AppointmentsUserAPI.php';

?>