<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 8:02 PM
 */

require_once dirname(__DIR__).'/library/Request/Request.php';

class AppointmentsAPIRequest extends AppointmentsRequest {

	public function __construct($lstrURL, $lstrMethod, array $lobjData) {

		// We don't need the http referrer for API calls.
		parent::__construct($lstrURL, '', $lstrMethod, $lobjData);

		$this->setHeaderOption(AppointmentsConstant::API_KEY_NAME, AppointmentsSDK::getKey());
		$this->setHeaderOption('V', AppointmentsConstant::API_VERSION);

		if(AppointmentsSDK::getService()->disableMessaging()){
			$this->setHeaderOption('Send-Messages', 'NO');
		}
	}
}