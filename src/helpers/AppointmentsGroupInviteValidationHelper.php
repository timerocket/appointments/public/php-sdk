<?php
require_once dirname(__DIR__) . '/api/AppointmentsAPIResponse.php';
require_once __DIR__ . '/AppointmentsPersonValidationHelper.php';

class AppointmentsGroupInviteValidationHelper {

	public static function validateInviteInputs($emails, $group_id) {
		return self::validateGetInviteStatusInputs($emails, $group_id);
	}

	public static function validateGetInviteStatusInputs($emails, $group_id) {
		if(empty(trim($emails))) {
			return AppointmentsAPIResponse::create(false, "emails field is required.");
		}

		$validGroupId = self::_validGroupId($group_id);
		if($validGroupId->success == false) {
			return $validGroupId;
		}

		return AppointmentsAPIResponse::create(true, '');
	}

	/**
	 * @param $group_id
	 * @param $auth_key
	 *
	 * @return AppointmentsAPIResponse
	 */
	public static function validateRejectInviteInputs($group_id, $auth_key) {

		if(trim($auth_key) === ''){
			return AppointmentsAPIResponse::create(true, 'Auth key cannot be blank');
		}

		$validGroupId = self::_validGroupId($group_id);
		if($validGroupId->success == false) {
			return $validGroupId;
		}

		return AppointmentsAPIResponse::create(true, '');
	}

	public static function validateAcceptInviteInputs($group_id, $auth_key) {

		if(trim($auth_key) === ''){
			return AppointmentsAPIResponse::create(false, 'Auth key cannot be blank');
		}

		$validGroupId = self::_validGroupId($group_id);
		if($validGroupId->success == false) {
			return $validGroupId;
		}

		return AppointmentsAPIResponse::create(true, '');
	}

	/**
	 * @param $group_id
	 *
	 * @return AppointmentsAPIResponse
	 */
	private static function _validGroupId($group_id) {

		if(empty($group_id)) {
			return AppointmentsAPIResponse::create(false, "group_id is required.");
		}

		if(!is_numeric($group_id)) {
			return AppointmentsAPIResponse::create(false, "group_id should be numeric.");
		}

		return AppointmentsAPIResponse::create(true, '');

	}
}