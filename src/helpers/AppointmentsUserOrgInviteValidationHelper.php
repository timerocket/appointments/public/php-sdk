<?php
require_once dirname(__DIR__) . '/api/AppointmentsAPIResponse.php';
require_once __DIR__ . '/AppointmentsPersonValidationHelper.php';

class AppointmentsUserOrgInviteValidationHelper {

	public static function validateInviteInputs($emails, $org_id) {
		return self::validateGetInviteStatusInputs($emails, $org_id);
	}

	public static function validateGetInviteStatusInputs($emails, $org_id) {
		if(empty(trim($emails))) {
			return AppointmentsAPIResponse::create(false, "emails field is required.");
		}

		$validOrgId = self::_validOrgId($org_id);
		if($validOrgId->success == false) {
			return $validOrgId;
		}

		return AppointmentsAPIResponse::create(true, "");
	}

	/**
	 * @param $org_id
	 * @param $auth_key
	 *
	 * @return AppointmentsAPIResponse
	 */
	public static function validateRejectInviteInputs($org_id, $auth_key) {

		if(trim($auth_key) === ''){
			return AppointmentsAPIResponse::create(true, 'Auth key cannot be blank');
		}

		$validOrgId = self::_validOrgId($org_id);
		if($validOrgId->success == false) {
			return $validOrgId;
		}

		return AppointmentsAPIResponse::create(true, '');
	}

	public static function validateAcceptInviteInputs($org_id, $auth_key) {

		if(trim($auth_key) === ''){
			return AppointmentsAPIResponse::create(false, 'Auth key cannot be blank');
		}

		$validOrgId = self::_validOrgId($org_id);
		if($validOrgId->success == false) {
			return $validOrgId;
		}

		return AppointmentsAPIResponse::create(true, '');
	}

	/**
	 * @param $org_id
	 *
	 * @return AppointmentsAPIResponse
	 */
	private static function _validOrgId($org_id) {

		if(empty($org_id)) {
			return AppointmentsAPIResponse::create(false, "org_id is required.");
		}

		if(!is_numeric($org_id)) {
			return AppointmentsAPIResponse::create(false, "org_id should be numeric.");
		}

		return AppointmentsAPIResponse::create(true, '');

	}
}