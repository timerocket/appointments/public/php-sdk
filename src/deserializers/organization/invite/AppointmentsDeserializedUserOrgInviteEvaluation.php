<?php

require_once __DIR__.'/AppointmentsDeserializedUserOrgInvite.php';

class AppointmentsDeserializedUserOrgInviteEvaluation {

	private $_email;
	private $_status;
	private $_userName;
	private $_invite;

	/**
	 * AppointmentsDeserializedUserOrgInvite constructor.
	 *
	 * @param $data stdClass
	 */
	public function __construct($data) {

		$this->_email         = $data->email;
		$this->_userName      = $data->user_name;
		$this->_status        = $data->status;

		if($data->invite !== null){
			$this->_invite = new AppointmentsDeserializedUserOrgInvite($data->invite);
		}else{
			$this->_invite = null;
		}
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->_email;
	}

	/**
	 * @return int
	 */
	public function getStatus() {
		return $this->_status;
	}

	public function couldBeInvited() {
		return in_array(
			$this->getStatus(), array(
                                  AppointmentsUserOrgInviteEvaluationStatus::UNINVITED_AND_NOT_APPOINTMENTS_USER,
                                  AppointmentsUserOrgInviteEvaluationStatus::UNINVITED_BUT_APPOINTMENTS_USER,
                                  AppointmentsUserOrgInviteEvaluationStatus::ALREADY_INVITED
		)
		);
	}

	public function willBeInvited() {
		return in_array(
			$this->getStatus(), array(
                                  AppointmentsUserOrgInviteEvaluationStatus::UNINVITED_AND_NOT_APPOINTMENTS_USER,
                                  AppointmentsUserOrgInviteEvaluationStatus::UNINVITED_BUT_APPOINTMENTS_USER
		)
		);
	}
}