<?php

require_once __DIR__.'/AppointmentsDeserializedUserOrgInviteEvaluation.php';

class AppointmentsDeserializedUserOrgInviteStatusResponseData {

	/**
	 * @var array AppointmentsDeserializedUserOrgInvite
	 */
	private $_userInviteStatuses;

	public function __construct($data) {
		foreach($data as $evaluation) {
			$this->_userInviteStatuses[] = new AppointmentsDeserializedUserOrgInviteEvaluation($evaluation);
		}
	}

	public function getUserOrgInviteStatuses() {
		return $this->_userInviteStatuses;
	}

}