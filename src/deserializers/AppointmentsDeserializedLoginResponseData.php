<?php

require_once __DIR__.'/AppointmentsDeserializedPerson.php';

class AppointmentsDeserializedLoginResponseData {

	/**
	 * @var AppointmentsDeserializedPerson
	 */
	private $_person;

	public function __construct($data) {
		$this->_person = new AppointmentsDeserializedPerson($data->person);
	}

	/**
	 * @return AppointmentsDeserializedPerson
	 */
	public function getPerson(){
		return $this->_person;
	}

}