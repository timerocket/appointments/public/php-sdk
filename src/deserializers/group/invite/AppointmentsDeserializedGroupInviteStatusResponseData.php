<?php

require_once __DIR__.'/AppointmentsDeserializedGroupInviteEvaluation.php';

class AppointmentsDeserializedGroupInviteStatusResponseData {

	/**
	 * @var array(AppointmentsDeserializedGroupInvite)
	 */
	private $_userInviteStatuses;

	public function __construct($data) {
		foreach($data as $item) {
			$this->_userInviteStatuses[] = new AppointmentsDeserializedGroupInviteEvaluation($item);
		}
	}

	/**
	 * @return array(AppointmentsDeserializedGroupInvite)
	 */
	public function getGroupInviteStatuses() {
		return $this->_userInviteStatuses;
	}

}