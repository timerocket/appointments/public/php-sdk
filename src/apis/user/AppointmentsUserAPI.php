<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 7:16 PM
 */

require_once dirname(dirname(__DIR__)).'/api/AppointmentsAPIRequest.php';
require_once __DIR__ . '/login/AppointmentsLoginRequest.php';
require_once __DIR__ . '/login/AppointmentsLoginResponse.php';

class AppointmentsUserAPI {

	const API = 'person';
	const METHOD_SIGN_IN_WITH_EMAIL = 'signInWithEmail';

	/**
	 * @param AppointmentsLoginRequest $request
	 *
	 * @return AppointmentsLoginResponse
	 */
	public static function login(AppointmentsLoginRequest $request){

		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_SIGN_IN_WITH_EMAIL),
			'GET',
			$request->getParameters()
		);

		$response = new AppointmentsLoginResponse($request->getResponse());

		return $response;
	}

}