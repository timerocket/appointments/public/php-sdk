<?php

require_once dirname(dirname(dirname(__DIR__))).'/deserializers/AppointmentsDeserializedLoginResponseData.php';

class AppointmentsLoginResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedLoginResponseData
	 */
	private $_deserializedData;

	/**
	 * AppointmentsLoginResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedLoginResponseData($this->data);
		}
	}

	public function getUserData(){
		return $this->_deserializedData;
	}
}