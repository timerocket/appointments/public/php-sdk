<?php
require_once dirname(dirname(dirname(__DIR__ ))). '/helpers/AppointmentsPersonValidationHelper.php';

class AppointmentsLoginRequest {

	private $_email;
	private $_password;


	public function __construct($email, $password) {
		$this->_email = $email;
		$this->_password = $password;
	}

	public function checkInputs() {
		return AppointmentsPersonValidationHelper::signInInputsValid($this->_email, $this->_password);
	}

	public function getParameters() {

		$encrypted_password = AppointmentsPasswordHelper::encryptPlainTextPassword($this->_password);

		$arr = array(
			'email' => $this->_email,
			'password' => $encrypted_password,
		);

		return $arr;
	}
}