<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 5/26/16
 * Time: 12:42 PM
 */

require_once dirname(dirname(__DIR__)) . '/api/AppointmentsAPIRequest.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteStatusRequest.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteStatusResponse.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteRequest.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteResponse.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteEvaluationStatus.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteStatus.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteRejectRequest.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteRejectResponse.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteAcceptRequest.php';
require_once __DIR__ . '/invite/AppointmentsGroupInviteAcceptResponse.php';

class AppointmentsGroupAPI {

	const API                                = 'group';
	const METHOD_GET_INVITE_STATUS_OF_EMAILS = 'getInviteStatusOfEmails';
	const METHOD_INVITE                      = 'invite';
	const METHOD_REJECT_INVITE               = 'rejectInvite';
	const METHOD_ACCEPT_INVITE               = 'acceptInvite';

	/**
	 * @param AppointmentsGroupInviteStatusRequest $request
	 *
	 * @return AppointmentsAPIResponse|AppointmentsGroupInviteStatusResponse
	 */
	public static function getInviteStatusOfEmails(AppointmentsGroupInviteStatusRequest $request) {

		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAuthorizedAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_GET_INVITE_STATUS_OF_EMAILS),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsGroupInviteStatusResponse($request->getResponse());

		return $response;
	}

	public static function invite(AppointmentsGroupInviteRequest $request) {

		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAuthorizedAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsGroupInviteResponse($request->getResponse());

		return $response;
	}

	/**
	 * @param AppointmentsGroupInviteRejectRequest $request
	 *
	 * @return AppointmentsAPIResponse|AppointmentsGroupInviteRejectResponse
	 */
	public static function rejectInvite(AppointmentsGroupInviteRejectRequest $request) {
		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_REJECT_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsGroupInviteRejectResponse($request->getResponse());

		return $response;
	}

	/**
	 * @param AppointmentsGroupInviteAcceptRequest $request
	 *
	 * @return AppointmentsAPIResponse|AppointmentsGroupInviteRejectResponse
	 */
	public static function acceptInvite(AppointmentsGroupInviteAcceptRequest $request) {

		$validInputs = $request->checkInputs();

		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_ACCEPT_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsGroupInviteAcceptResponse($request->getResponse());

		return $response;
	}

}