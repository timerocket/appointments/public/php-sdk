<?php

require_once dirname(dirname(dirname(__DIR__))).'/deserializers/group/invite/AppointmentsDeserializedGroupInviteResponseData.php';

class AppointmentsGroupInviteResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedGroupInviteResponseData
	 */
	private $_deserializedData;

	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedGroupInviteResponseData($this->data);
		}
	}

	public function getGroupInviteData(){
		return $this->_deserializedData;
	}
}