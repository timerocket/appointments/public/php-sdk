<?php

require_once dirname(dirname(dirname(__DIR__))).'/deserializers/group/invite/AppointmentsDeserializedGroupInviteStatusResponseData.php';

class AppointmentsGroupInviteStatusResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedGroupInviteStatusResponseData
	 */
	private $_deserializedData;

	/**
	 * AppointmentsGroupInviteStatusResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedGroupInviteStatusResponseData($this->data);
		}
	}

	/**
	 * @return AppointmentsDeserializedGroupInviteStatusResponseData|null
	 */
	public function getGroupInviteData(){
		return $this->_deserializedData;
	}
}