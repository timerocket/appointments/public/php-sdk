<?php

require_once dirname(dirname(dirname(__DIR__))) . '/deserializers/group/invite/AppointmentsDeserializedGroupInvite.php';

class AppointmentsGroupInviteRejectResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedGroupInvite
	 */
	private $_deserializedData;

	/**
	 * AppointmentsGroupInviteRejectResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedGroupInvite($this->data);
		}
	}

	/**
	 * @return AppointmentsDeserializedGroupInvite
	 */
	public function getGroupInvite(){
		return $this->_deserializedData;
	}
}