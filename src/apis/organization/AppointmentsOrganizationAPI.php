<?php

require_once dirname(dirname(__DIR__)) . '/api/AppointmentsAPIRequest.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteStatusRequest.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteStatusResponse.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteRequest.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteResponse.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteStatus.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteEvaluationStatus.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteRejectRequest.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteRejectResponse.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteAcceptRequest.php';
require_once __DIR__ . '/invite/AppointmentsUserOrgInviteAcceptResponse.php';

class AppointmentsOrganizationAPI {

	const API                                = 'organization';
	const METHOD_GET_INVITE_STATUS_OF_EMAILS = 'getInviteStatusOfEmails';
	const METHOD_INVITE                      = 'invite';
	const METHOD_REJECT_INVITE               = 'rejectInvite';
	const METHOD_ACCEPT_INVITE               = 'acceptInvite';
	
	public static function getInviteStatusOfEmails(AppointmentsUserOrgInviteStatusRequest $request) {

		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAuthorizedAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_GET_INVITE_STATUS_OF_EMAILS),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsUserOrgInviteStatusResponse($request->getResponse());

		return $response;
	}

	public static function invite(AppointmentsUserOrgInviteRequest $request) {

		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAuthorizedAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsUserOrgInviteResponse($request->getResponse());

		return $response;
	}

	/**
	 * @param AppointmentsUserOrgInviteRejectRequest $request
	 *
	 * @return AppointmentsAPIResponse|AppointmentsUserOrgInviteRejectResponse
	 */
	public static function rejectInvite(AppointmentsUserOrgInviteRejectRequest $request) {
		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_REJECT_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsUserOrgInviteRejectResponse($request->getResponse());

		return $response;
	}

	/**
	 * @param AppointmentsUserOrgInviteAcceptRequest $request
	 *
	 * @return AppointmentsAPIResponse|AppointmentsUserOrgInviteRejectResponse
	 */
	public static function acceptInvite(AppointmentsUserOrgInviteAcceptRequest $request) {
		$validInputs = $request->checkInputs();
		if(!$validInputs->success) {
			return $validInputs;
		}

		$request = new AppointmentsAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, self::METHOD_ACCEPT_INVITE),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		$response = new AppointmentsUserOrgInviteAcceptResponse($request->getResponse());

		return $response;
	}

}