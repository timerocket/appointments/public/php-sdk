<?php
require_once dirname(dirname(dirname(__DIR__))) . '/helpers/AppointmentsUserOrgInviteValidationHelper.php';

class AppointmentsUserOrgInviteAcceptRequest {

	private $_orgId;
	private $_authKey;

	public function __construct($org_id, $auth_key) {
		$this->_orgId   = $org_id;
		$this->_authKey = $auth_key;
	}

	public function checkInputs() {
		return AppointmentsUserOrgInviteValidationHelper::validateAcceptInviteInputs($this->_orgId, $this->_authKey);
	}

	public function getParameters() {

		$arr = array(
			'org_id'   => $this->_orgId,
			'auth_key' => $this->_authKey,
		);

		return $arr;
	}
}