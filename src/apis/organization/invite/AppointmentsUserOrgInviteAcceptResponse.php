<?php

require_once dirname(dirname(dirname(__DIR__))) . '/deserializers/organization/invite/AppointmentsDeserializedUserOrgInvite.php';

class AppointmentsUserOrgInviteAcceptResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedUserOrgInvite
	 */
	private $_deserializedData;

	/**
	 * AppointmentsUserOrgInviteAcceptResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedUserOrgInvite($this->data);
		}
	}

	public function getOrgInviteEvaluation(){
		return $this->_deserializedData;
	}
}