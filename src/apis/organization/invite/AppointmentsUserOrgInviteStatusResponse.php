<?php

require_once dirname(dirname(dirname(__DIR__))).'/deserializers/organization/invite/AppointmentsDeserializedUserOrgInviteStatusResponseData.php';

class AppointmentsUserOrgInviteStatusResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedUserOrgInviteStatusResponseData
	 */
	private $_deserializedData;

	/**
	 * AppointmentsUserOrgInviteStatusResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedUserOrgInviteStatusResponseData($this->data);
		}
	}

	/**
	 * @return AppointmentsDeserializedUserOrgInviteStatusResponseData|null
	 */
	public function getUserOrgInviteData(){
		return $this->_deserializedData;
	}
}