<?php

require_once dirname(dirname(dirname(__DIR__))) . '/deserializers/organization/invite/AppointmentsDeserializedUserOrgInviteResponseData.php';

class AppointmentsUserOrgInviteResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedUserOrgInviteResponseData
	 */
	private $_deserializedData;

	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedUserOrgInviteResponseData($this->data);
		}
	}

	public function getUserOrgInviteData(){
		return $this->_deserializedData;
	}
}