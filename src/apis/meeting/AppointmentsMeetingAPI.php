<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 7:16 PM
 */

require_once dirname(dirname(__DIR__)).'/api/AppointmentsAPIRequest.php';
require_once __DIR__.'/create/AppointmentsCreateMeetingRequest.php';
require_once __DIR__.'/create/AppointmentsCreateMeetingResponse.php';

class AppointmentsMeetingAPI {

	const API = 'meeting';

	/**
	 * @param AppointmentsCreateMeetingRequest $request
	 *
	 * @return AppointmentsCreateMeetingResponse
	 */
	public static function create(AppointmentsCreateMeetingRequest $request){

		$request = new AppointmentsAuthorizedAPIRequest(
			AppointmentsSDK::getEndpoint(self::API, ''),
			'POST',
			$request->getParameters()
		);

		$request->makeJSONRequest();

		return new AppointmentsCreateMeetingResponse($request->getResponse());
	}

}