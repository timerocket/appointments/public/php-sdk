<?php
/**
 * Created on a Mac. Probably won't work on Windows.
 * User: josh
 * Date: 4/15/16
 * Time: 8:45 PM
 */

require_once dirname(dirname(dirname(__DIR__))).'/deserializers/AppointmentsDeserializedCreateMeetingResponseData.php';

class AppointmentsCreateMeetingResponse extends AppointmentsAPIResponse {

	/**
	 * @var AppointmentsDeserializedCreateMeetingResponseData
	 */
	private $_deserializedData;

	/**
	 * AppointmentsCreateMeetingResponse constructor.
	 *
	 * @param AppointmentsResponse $response
	 */
	public function __construct(AppointmentsResponse $response) {
		parent::__construct($response);

		$this->_deserializedData = null;

		if($this->data !== null){
			$this->_deserializedData = new AppointmentsDeserializedCreateMeetingResponseData($this->data);
		}
	}

	public function getMeetingData(){
		return $this->_deserializedData;
	}
}